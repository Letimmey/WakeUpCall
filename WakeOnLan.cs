﻿using System;
using System.Net;
using System.Net.Sockets;

namespace WakeUpCall
{
    public static class WakeOnLan
    {
        public static void WakeUp(string macAddress, IPAddress ipAddress)
        {
            UdpClient client = new UdpClient();

            Byte[] datagram = new byte[102];

            for (int i = 0; i <= 5; i++)
            {
                datagram[i] = 0xff;
            }

            string[] macDigits = null;
            if (macAddress.Contains("-"))
            {
                macDigits = macAddress.Split('-');
            }
            else
            {
                macDigits = macAddress.Split(':');
            }

            if (macDigits.Length != 6)
            {
                throw new ArgumentException("Incorrect MAC address supplied!");
            }

            int start = 6;
            for (int i = 0; i < 16; i++)
            {
                for (int x = 0; x < 6; x++)
                {
                    datagram[start + i * 6 + x] = (byte)Convert.ToInt32(macDigits[x], 16);
                }
            }

            IPAddress mask = GetSubnetmask(ipAddress);
            IPAddress broadcastAddress = ipAddress.GetBroadcastAddress(mask);

            client.Send(datagram, datagram.Length, broadcastAddress.ToString(), 3);
            client.Dispose();
        }

        private static IPAddress GetSubnetmask(IPAddress ipaddress)
        {
            string octet = string.Empty;

            uint firstOctet = GetFirstOctet(ipaddress);
            if (firstOctet >= 0 && firstOctet <= 127)
                octet = "255.0.0.0";
            else if (firstOctet >= 128 && firstOctet <= 191)
                octet = "255.255.0.0";
            else if (firstOctet >= 192 && firstOctet <= 223)
                octet = "255.255.255.0";
            else octet = "0.0.0.0";

            return IPAddress.Parse(octet);
        }

        private static uint GetFirstOctet(IPAddress ipAddress)
        {
            byte[] byteIP = ipAddress.GetAddressBytes();
            uint ipInUint = byteIP[0];
            return ipInUint;
        }
    }
}
