﻿using Newtonsoft.Json;

namespace WakeUpCall
{
  public class HostObject
  {
    [JsonProperty("hostname")]
    public string HostName { get; set; }

    [JsonProperty("hostmacaddress")]
    public string HostMacAddress { get; set; }
  }
}
