﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace WakeUpCall
{
    public static class MacAddressHelper
    {
        [System.Runtime.InteropServices.DllImport("Iphlpapi.dll", EntryPoint = "SendARP")]
        internal static extern Int32 SendArp(Int32 destIpAddress, Int32 srcIpAddress, byte[] macAddress, ref Int32 macAddressLength);

        public static PhysicalAddress GetMacFromIp(IPAddress srcAddr, IPAddress ipAddr)
        {
            if (ipAddr.AddressFamily != AddressFamily.InterNetwork)
                throw new ArgumentException("Given IP-Address is not IPv4");

            int addrInt = IpToInt(ipAddr);
            int srcAddrInt = IpToInt(srcAddr);

            byte[] mac = new byte[6];
            int length = mac.Length;

            int reply = SendArp(addrInt, srcAddrInt, mac, ref length);

            if (reply != 0)
                throw new System.ComponentModel.Win32Exception(reply);

            return new PhysicalAddress(mac);
        }

        private static Int32 IpToInt(IPAddress ip)
        {
            byte[] bytes = ip.GetAddressBytes();           
            return BitConverter.ToInt32(bytes, 0);
        }
    }
}
