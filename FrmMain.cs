﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Newtonsoft.Json;
using WakeUpCall.Properties;

namespace WakeUpCall
{
  public partial class FrmMain : Form
  {
    #region member

    private BindingList<HostObject> _hostObjects;

    private IPAddress _srcIpAddress;
    private IPAddress _hostAddress;
    private string _hostMacAddress;
    private string _hostName = string.Empty;

    #endregion

    #region constructor

    public FrmMain()
    {
      InitializeComponent();
      btWOL.Enabled = !string.IsNullOrEmpty(tbMacAddr.Text);
      btOpenRdp.Enabled = !string.IsNullOrEmpty(tbMacAddr.Text);
    }

    #endregion

    #region private

    private bool IsIpAddress(string ipAddr)
    {
      Regex ipRegex = new Regex(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b");
      return ipRegex.IsMatch(ipAddr);
    }

    private IPAddress GetLocalIpAddress()
    {
      IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
      foreach (IPAddress ip in host.AddressList)
        if (ip.AddressFamily == AddressFamily.InterNetwork)
          return ip;
      return IPAddress.None;
    }

    private void GetMacAddress(IPAddress hostIp)
    {
      _srcIpAddress = GetLocalIpAddress();
      PhysicalAddress hostMacAddress = MacAddressHelper.GetMacFromIp(_srcIpAddress, hostIp);
      string macString = string.Join(":", hostMacAddress.GetAddressBytes().Select(x => x.ToString("X2")));
      tbMacAddr.Text = macString;
      _hostMacAddress = macString;
      btAddHost.Enabled = true;
    }

    private void SaveHostsToFile()
    {
      string appDirectory = Path.GetDirectoryName(Application.ExecutablePath);
      if (string.IsNullOrEmpty(appDirectory)) return;

      string fileName = Settings.Default.SaveFileName;
      string file = Path.Combine(appDirectory, fileName);

      if (!CheckForSaveFile(appDirectory))
      {
        try
        {
          using (FileStream stream = new FileStream(Path.Combine(appDirectory, Settings.Default.SaveFileName), FileMode.Create))
            stream.Flush();
        }
        catch (Exception e)
        {
          MessageBox.Show(e.Message, @"Fehler", MessageBoxButtons.OK);
          return;
        }
      }

      FileStream fs = File.Open(file, FileMode.Truncate);
      using (StreamWriter sw = new StreamWriter(fs))
      {
        JsonSerializer serializer = new JsonSerializer();
        serializer.Serialize(sw, _hostObjects);
      }
      fs.Close();
    }

    private bool CheckForSaveFile(string appDirectory)
    {
      return File.Exists(Path.Combine(appDirectory, Settings.Default.SaveFileName));
    }

    private BindingList<HostObject> GetSavedHostObjects()
    {
      if (!CheckForSaveFile(Path.GetDirectoryName(Application.ExecutablePath))) return new BindingList<HostObject>();
      string fs = File.ReadAllText(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), Settings.Default.SaveFileName));

      if (string.IsNullOrEmpty(fs)) return new BindingList<HostObject>();

      return JsonConvert.DeserializeObject<BindingList<HostObject>>(fs);
    }

    private void InitListBox()
    {
      _hostObjects = GetSavedHostObjects();

      lbHosts.DisplayMember = "Hostname";
      lbHosts.ValueMember = "HostMacAddress";
      lbHosts.DataSource = _hostObjects;
    }

    private bool HostIsDuplicate()
    {
      if (_hostObjects == null) return false;
      
      return _hostObjects.Any(x => x.HostName.Equals(_hostName) || x.HostMacAddress.Equals(_hostMacAddress));
    }

    private void RemoveListEntry(int idx)
    {
      if (_hostObjects.Count == 0) return;
      _hostObjects.RemoveAt(idx);

      tbMacAddr.Text = string.Empty;
      btRemoveEntry.Enabled = false;
      SaveHostsToFile();
    }

    #endregion

    #region events

    private void btWOL_Click(object sender, EventArgs e)
    {
      string macAddress = tbMacAddr.Text;
      IPAddress localIp = GetLocalIpAddress();

      WakeOnLan.WakeUp(macAddress, localIp);
    }

    private void btGetHost_Click(object sender, EventArgs e)
    {
      if (string.IsNullOrWhiteSpace(tbHostName.Text))
      {
        MessageBox.Show(@"Didn't you forget something?", @"No Host/IP-Address given", MessageBoxButtons.OK, MessageBoxIcon.Question);
        return;
      }
      string hostName = tbHostName.Text;
      if (!IsIpAddress(hostName))
        hostName = hostName.ToUpper();

      try
      {
        IPHostEntry hostEntry = Dns.GetHostEntry(hostName);

        _hostName = !hostEntry.HostName.Contains(".") ? hostEntry.HostName : hostEntry.HostName.Substring(0, hostEntry.HostName.IndexOf('.'));
        _hostAddress = Array.Find(hostEntry.AddressList, x => x.AddressFamily.Equals(AddressFamily.InterNetwork));

        GetMacAddress(_hostAddress);
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex);
        MessageBox.Show(ex.Message, @"Oops!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }

      btAddHost.Enabled = true;
    }

    private void frmMain_Load(object sender, EventArgs e)
    {
      InitListBox();
      notifyIcon1.Visible = false;
    }

    private void tbHostName_TextChanged(object sender, EventArgs e)
    {
      btGetHost.Enabled = true;
    }

    private void lbHosts_Click(object sender, EventArgs e)
    {
      if (lbHosts.Items.Count < 1) return;
      tbMacAddr.Text = lbHosts.SelectedValue.ToString();
      btRemoveEntry.Enabled = true;
    }

    private void btExit_Click(object sender, EventArgs e)
    {
      Dispose();
    }

    private void btOpenRdp_Click(object sender, EventArgs e)
    {
      try
      {
        HostObject host = (HostObject)lbHosts.SelectedItem;
        string hostName = host.HostName;

        Process rdpProcess = new Process
        {
          StartInfo =
          {
            FileName = "mstsc.exe",
            Arguments = $"/v {hostName} /f"
          }
        };
        rdpProcess.Start();
      }
      catch (Exception exception)
      {
        Console.WriteLine(exception);
        MessageBox.Show(exception.Message, @"Oops!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
    }

    private void FrmMain_Resize(object sender, EventArgs e)
    {
      if (WindowState == FormWindowState.Minimized)
      {
        notifyIcon1.Visible = true;
        notifyIcon1.BalloonTipText = @"Wake up call is still running.";
        notifyIcon1.ShowBalloonTip(500);
        Hide();
      }
      else if (WindowState == FormWindowState.Normal)
      {
        notifyIcon1.Visible = false;
      }
    }

    private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      this.Show();
      this.WindowState = FormWindowState.Normal;
      this.BringToFront();
      notifyIcon1.Visible = false;
    }

    private void tbMacAddr_TextChanged(object sender, EventArgs e)
    {
      btWOL.Enabled = !string.IsNullOrEmpty(tbMacAddr.Text);
      btOpenRdp.Enabled = !string.IsNullOrEmpty(tbMacAddr.Text);
    }

    private void btRemoveEntry_Click(object sender, EventArgs e)
    {
      RemoveListEntry(lbHosts.SelectedIndex);
    }

    #endregion

    private void btAddHost_Click(object sender, EventArgs e)
    {
      if (_hostName == string.Empty && MessageBox.Show(@"Cannot save host, no host name provided", @"Error", MessageBoxButtons.OK) == DialogResult.OK) return;
      if (_hostMacAddress == string.Empty && MessageBox.Show(@"Cannot save host, no host mac address provided", @"Error", MessageBoxButtons.OK) == DialogResult.OK) return;

      if (!HostIsDuplicate())
      {
        if (_hostObjects == null)
          _hostObjects = new BindingList<HostObject>();

        _hostObjects.Add(new HostObject { HostName = _hostName, HostMacAddress = _hostMacAddress });

        SaveHostsToFile();
        InitListBox();
      }
      else
      {
        MessageBox.Show(@"This host is already in the list", @"Duplicate found", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

      btAddHost.Enabled = false;
    }
  }
}