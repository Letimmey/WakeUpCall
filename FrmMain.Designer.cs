﻿namespace WakeUpCall
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
      this.tbMacAddr = new System.Windows.Forms.TextBox();
      this.btWOL = new System.Windows.Forms.Button();
      this.tbHostName = new System.Windows.Forms.TextBox();
      this.lblHost = new System.Windows.Forms.Label();
      this.lblMacAddress = new System.Windows.Forms.Label();
      this.btGetHost = new System.Windows.Forms.Button();
      this.btAddHost = new System.Windows.Forms.Button();
      this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
      this.lbHosts = new System.Windows.Forms.ListBox();
      this.lblSavedHosts = new System.Windows.Forms.Label();
      this.btExit = new System.Windows.Forms.Button();
      this.btOpenRdp = new System.Windows.Forms.Button();
      this.btRemoveEntry = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // tbMacAddr
      // 
      this.tbMacAddr.Location = new System.Drawing.Point(15, 111);
      this.tbMacAddr.Name = "tbMacAddr";
      this.tbMacAddr.ReadOnly = true;
      this.tbMacAddr.Size = new System.Drawing.Size(159, 20);
      this.tbMacAddr.TabIndex = 4;
      this.tbMacAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.tbMacAddr.TextChanged += new System.EventHandler(this.tbMacAddr_TextChanged);
      // 
      // btWOL
      // 
      this.btWOL.Location = new System.Drawing.Point(15, 137);
      this.btWOL.Name = "btWOL";
      this.btWOL.Size = new System.Drawing.Size(159, 23);
      this.btWOL.TabIndex = 5;
      this.btWOL.Text = "Wake up selected Host";
      this.btWOL.UseVisualStyleBackColor = true;
      this.btWOL.Click += new System.EventHandler(this.btWOL_Click);
      // 
      // tbHostName
      // 
      this.tbHostName.Location = new System.Drawing.Point(12, 25);
      this.tbHostName.Name = "tbHostName";
      this.tbHostName.Size = new System.Drawing.Size(159, 20);
      this.tbHostName.TabIndex = 1;
      this.tbHostName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.tbHostName.TextChanged += new System.EventHandler(this.tbHostName_TextChanged);
      // 
      // lblHost
      // 
      this.lblHost.AutoSize = true;
      this.lblHost.Location = new System.Drawing.Point(9, 9);
      this.lblHost.Name = "lblHost";
      this.lblHost.Size = new System.Drawing.Size(91, 13);
      this.lblHost.TabIndex = 0;
      this.lblHost.Text = "Host (Name or IP)";
      // 
      // lblMacAddress
      // 
      this.lblMacAddress.AutoSize = true;
      this.lblMacAddress.Location = new System.Drawing.Point(12, 95);
      this.lblMacAddress.Name = "lblMacAddress";
      this.lblMacAddress.Size = new System.Drawing.Size(71, 13);
      this.lblMacAddress.TabIndex = 3;
      this.lblMacAddress.Text = "MAC-Address";
      // 
      // btGetHost
      // 
      this.btGetHost.Location = new System.Drawing.Point(12, 51);
      this.btGetHost.Name = "btGetHost";
      this.btGetHost.Size = new System.Drawing.Size(159, 23);
      this.btGetHost.TabIndex = 2;
      this.btGetHost.Text = "Get Host";
      this.btGetHost.UseVisualStyleBackColor = true;
      this.btGetHost.Click += new System.EventHandler(this.btGetHost_Click);
      // 
      // btAddHost
      // 
      this.btAddHost.Enabled = false;
      this.btAddHost.Location = new System.Drawing.Point(15, 223);
      this.btAddHost.Name = "btAddHost";
      this.btAddHost.Size = new System.Drawing.Size(159, 23);
      this.btAddHost.TabIndex = 6;
      this.btAddHost.Text = "Add Host to List";
      this.btAddHost.UseVisualStyleBackColor = true;
      this.btAddHost.Click += new System.EventHandler(this.btAddHost_Click);
      // 
      // notifyIcon1
      // 
      this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
      this.notifyIcon1.Text = "Wake up call";
      this.notifyIcon1.Visible = true;
      this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
      // 
      // lbHosts
      // 
      this.lbHosts.FormattingEnabled = true;
      this.lbHosts.Location = new System.Drawing.Point(200, 25);
      this.lbHosts.Name = "lbHosts";
      this.lbHosts.Size = new System.Drawing.Size(159, 186);
      this.lbHosts.TabIndex = 7;
      this.lbHosts.Click += new System.EventHandler(this.lbHosts_Click);
      // 
      // lblSavedHosts
      // 
      this.lblSavedHosts.AutoSize = true;
      this.lblSavedHosts.Location = new System.Drawing.Point(200, 9);
      this.lblSavedHosts.Name = "lblSavedHosts";
      this.lblSavedHosts.Size = new System.Drawing.Size(68, 13);
      this.lblSavedHosts.TabIndex = 8;
      this.lblSavedHosts.Text = "Saved Hosts";
      // 
      // btExit
      // 
      this.btExit.Location = new System.Drawing.Point(200, 262);
      this.btExit.Name = "btExit";
      this.btExit.Size = new System.Drawing.Size(159, 23);
      this.btExit.TabIndex = 9;
      this.btExit.Text = "Exit";
      this.btExit.UseVisualStyleBackColor = true;
      this.btExit.Click += new System.EventHandler(this.btExit_Click);
      // 
      // btOpenRdp
      // 
      this.btOpenRdp.Location = new System.Drawing.Point(15, 167);
      this.btOpenRdp.Name = "btOpenRdp";
      this.btOpenRdp.Size = new System.Drawing.Size(159, 23);
      this.btOpenRdp.TabIndex = 10;
      this.btOpenRdp.Text = "Connect via RDP";
      this.btOpenRdp.UseVisualStyleBackColor = true;
      this.btOpenRdp.Click += new System.EventHandler(this.btOpenRdp_Click);
      // 
      // btRemoveEntry
      // 
      this.btRemoveEntry.Enabled = false;
      this.btRemoveEntry.Location = new System.Drawing.Point(200, 223);
      this.btRemoveEntry.Name = "btRemoveEntry";
      this.btRemoveEntry.Size = new System.Drawing.Size(159, 23);
      this.btRemoveEntry.TabIndex = 11;
      this.btRemoveEntry.Text = "Remove Host from List";
      this.btRemoveEntry.UseVisualStyleBackColor = true;
      this.btRemoveEntry.Click += new System.EventHandler(this.btRemoveEntry_Click);
      // 
      // FrmMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(371, 297);
      this.Controls.Add(this.btRemoveEntry);
      this.Controls.Add(this.btOpenRdp);
      this.Controls.Add(this.btExit);
      this.Controls.Add(this.lblSavedHosts);
      this.Controls.Add(this.lbHosts);
      this.Controls.Add(this.btAddHost);
      this.Controls.Add(this.btGetHost);
      this.Controls.Add(this.lblMacAddress);
      this.Controls.Add(this.lblHost);
      this.Controls.Add(this.tbHostName);
      this.Controls.Add(this.btWOL);
      this.Controls.Add(this.tbMacAddr);
      this.MaximizeBox = false;
      this.Name = "FrmMain";
      this.Text = "Wake Up Call";
      this.Load += new System.EventHandler(this.frmMain_Load);
      this.Resize += new System.EventHandler(this.FrmMain_Resize);
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbMacAddr;
        private System.Windows.Forms.Button btWOL;
        private System.Windows.Forms.TextBox tbHostName;
        private System.Windows.Forms.Label lblHost;
        private System.Windows.Forms.Label lblMacAddress;
        private System.Windows.Forms.Button btGetHost;
        private System.Windows.Forms.Button btAddHost;
    private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ListBox lbHosts;
    private System.Windows.Forms.Label lblSavedHosts;
    private System.Windows.Forms.Button btExit;
    private System.Windows.Forms.Button btOpenRdp;
    private System.Windows.Forms.Button btRemoveEntry;
  }
}

